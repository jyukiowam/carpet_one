//Function definition for header scroll handler
//Initial scroll position
let prevScrollpos = window.scrollY;

//Flag to check if menu is hidden
let up = false;

//Flag for requestAnimationFrame limiting
let update = false;


/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
const handleScroll = () => {
    const currentScrollPos = $(window).scrollTop();
    const headerHeight = $("#global-nav").outerHeight();

    if (window.innerWidth < 992 && currentScrollPos > headerHeight) {
        $("body").css("padding-top", `${headerHeight}px`);
        $("#global-nav").addClass("position-fixed");

        hideHeader(currentScrollPos);
    } else if (window.innerWidth < 992 && currentScrollPos == 0) {
        $("#global-nav").removeClass("position-fixed");
        $("#global-nav").css("top", "");
        $("body").css("padding-top", "");
    } else if (window.innerWidth >= 992) {
        $("body").css("padding-top", "");

        hideHeader(currentScrollPos);

        if(currentScrollPos == 0){
            $("#global-nav").css("top", "0");
        }

        if ($("#global-nav").hasClass("header--gradient-toggle")) {
            if (currentScrollPos == 0) {
                $("#global-nav").addClass("header--gradient");
            } else if ($("#global-nav").hasClass("header--gradient")) {
                $("#global-nav").removeClass("header--gradient");

            }
        }

    }

    //Updates value of last known scroll position for next tick
    prevScrollpos = currentScrollPos;

};

const hideHeader = (currentScroll) => {
    if (up && prevScrollpos > currentScroll) {
        $("#global-nav").css("top", "0");
        up = !up;
    } else if (!up && currentScroll > prevScrollpos) {
        $(".header__hamburger[aria-expanded='true']").click().blur();
        $("#global-nav").css("top", "");
        up = !up;
        $("#global-nav").removeClass("header--initial");

    }
}

const updateAnimationTick = () => {
    //Resets update so we can capture next onScroll
    update = false;
    handleScroll();
}

const requestTick = () => {
    if (!update) {
        requestAnimationFrame(updateAnimationTick)
        update = true;
    }
}


$(document).ready(function () {
    //Line clamp for text elements
    $(".clamp").each((index, element) => {
        const maxLines = element.dataset.clamp;
        $clamp(element, {clamp: maxLines, useNativeClamp: true});
    });

    $(".carousel-controls, a").each((index, element) => {
        $(element).on("click", (event) => {
            event.target.blur();
        });
    })

    //Removes CTA banner from DOM
    $(".cta-banner__dismiss").on("click", (event) => {
        $(event.target).closest(".cta-banner--dismissable").remove();
    })

    //Handles header scroll events:
    // Hides header, toggles gradient on desktop
    $(window).on("scroll", requestTick);
    $(window).on("resize", requestTick);

    $(window).trigger("scroll");
})